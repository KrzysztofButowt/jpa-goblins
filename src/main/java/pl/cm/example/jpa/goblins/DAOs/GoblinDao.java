package pl.cm.example.jpa.goblins.DAOs;

import pl.cm.example.jpa.goblins.Cave;
import pl.cm.example.jpa.goblins.Goblin;
import pl.cm.example.jpa.goblins.Goblin_;
import pl.cm.example.jpa.goblins.Weapon;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class GoblinDao {

    public List<Goblin> findAllGoblins (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Goblin> query = builder.createQuery(Goblin.class);
        Root<Goblin> goblin = query.from(Goblin.class);
        query.select(goblin);
        List <Goblin> goblins =  em.createQuery(query).getResultList();

        em.getTransaction().commit();
        em.close();
        return goblins;
    }

    public long countCaves (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        String queryString = "SELECT COUNT(g) FROM Goblin g";
        Query query = em.createQuery(queryString, Long.class);
        Long count = (Long) query.getSingleResult();
        em.getTransaction().commit();
        em.close();
        return count;
    }

    public List<Goblin> homelessGoblins (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Goblin> query = builder.createQuery(Goblin.class);
        Root<Goblin> goblin = query.from(Goblin.class);
        query.select(goblin).where(builder.isNull(goblin.get(Goblin_.cave)));
        List <Goblin> goblins =  em.createQuery(query).getResultList();

        em.getTransaction().commit();
        em.close();
        return goblins;
    }

    public List<Goblin> weaponlessGoblins (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        String queryString = "SELECT g FROM Goblin g WHERE g.weapon  IS NULL";
        TypedQuery<Goblin> query = em.createQuery(queryString, Goblin.class);
        List <Goblin> goblins = query.getResultList();
        em.getTransaction().commit();
        em.close();
        return goblins;
    }

    public List<Cave> emptyCaves (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        String queryString = "SELECT g.cave FROM Goblin g WHERE g.cave.id IS EMPTY ";
        TypedQuery<Cave > query = em.createQuery(queryString, Cave.class);
        List <Cave> caves = query.getResultList();
        em.getTransaction().commit();
        em.close();
        return caves;
    }
}
