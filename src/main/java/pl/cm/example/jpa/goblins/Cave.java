package pl.cm.example.jpa.goblins;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString (exclude = "goblins")
@EqualsAndHashCode
@Entity
public class Cave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Setter
    @OneToMany (mappedBy = "cave", fetch = FetchType.EAGER)
    private List<Goblin> goblins = new ArrayList<>();


}
