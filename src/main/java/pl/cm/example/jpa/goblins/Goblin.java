package pl.cm.example.jpa.goblins;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class Goblin {

    @Id
    private String name;

    @OneToOne
    @JoinColumn
    private Weapon weapon;

    @ManyToOne
    private Cave cave;

    public Goblin(String name) {
        this.name = name;
    }
}
