package pl.cm.example.jpa.goblins.DAOs;

import pl.cm.example.jpa.goblins.Cave;
import pl.cm.example.jpa.goblins.Goblin;
import pl.cm.example.jpa.goblins.Weapon;
import pl.cm.example.jpa.goblins.Weapon_;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

public class WeaponDao {
    public List<Weapon> findAllWeapons (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Weapon> query = builder.createQuery(Weapon.class);
        Root<Weapon> weapon = query.from(Weapon.class);
        query.select(weapon);
        List <Weapon> weapons =  em.createQuery(query).getResultList();

        em.getTransaction().commit();
        em.close();
        return weapons;
    }
    public long countWeapons (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        String queryString = "SELECT COUNT(w) FROM Weapon w";
        Query query = em.createQuery(queryString, Long.class);
        Long count = (Long) query.getSingleResult();
        em.getTransaction().commit();
        em.close();
        return count;
    }

    public List<Weapon> findWeaponWithoutOwner (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Weapon> query = builder.createQuery(Weapon.class);
        Root<Weapon> weapon = query.from(Weapon.class);
        Join<Weapon, Goblin> join = weapon.join(Weapon_.goblin, JoinType.LEFT);

        query.select(weapon).where(builder.isNull(join));
        List <Weapon> weapons =  em.createQuery(query).getResultList();

        em.getTransaction().commit();
        em.close();
        return weapons;
    }
}
