package pl.cm.example.jpa.goblins;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString (exclude = "goblin")
@EqualsAndHashCode
@Entity
public class Weapon {

    @Id
    private String name;

    @OneToOne (mappedBy = "weapon")
    private Goblin goblin;

    public Weapon(String name) {
        this.name = name;
    }
}
