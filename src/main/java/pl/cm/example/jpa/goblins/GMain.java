package pl.cm.example.jpa.goblins;


import pl.cm.example.jpa.goblins.DAOs.CaveDao;
import pl.cm.example.jpa.goblins.DAOs.GoblinDao;
import pl.cm.example.jpa.goblins.DAOs.WeaponDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class GMain {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookPu");
        EntityManager em;

        em = emf.createEntityManager();
        em.getTransaction().begin();

        Weapon w1 = new Weapon("Bron1");
        Weapon w2 = new Weapon("Bron2");
        Weapon w3 = new Weapon("Bron3");

        Cave fullCave = new Cave();
        Cave emptyCave = new Cave();

        Goblin g1 = new Goblin("Imie1");
        Goblin g2 = new Goblin("Imie2");
        Goblin g3 = new Goblin("Imie3");
        Goblin g4 = new Goblin("Imie4");
        Goblin g5 = new Goblin("Imie5");

        g1.setWeapon(w1);
        g2.setWeapon(w2);


        g1.setCave(fullCave);
        g2.setCave(fullCave);
        g3.setCave(fullCave);
        g4.setCave(fullCave);

        em.persist(w1);
        em.persist(w2);
        em.persist(w3);

        em.persist(fullCave);
        em.persist(emptyCave);

        em.persist(g1);
        em.persist(g2);
        em.persist(g3);
        em.persist(g4);
        em.persist(g5);

        em.getTransaction().commit();
        em.close();


        EntityManager em1;

        em1 = emf.createEntityManager();
        em1.getTransaction().begin();

        Cave empty = em1.find(Cave.class, 2);
        Cave full = em1.find(Cave.class, 1);

        Weapon w1_ro =em1.find(Weapon.class, "Bron1");
        Weapon w2_ro =em1.find(Weapon.class, "Bron2");
        Weapon w3_ro =em1.find(Weapon.class, "Bron3");

        Goblin g1_ro = em1.find(Goblin.class, "Imie1");
        Goblin g2_ro = em1.find(Goblin.class, "Imie2");
        Goblin g3_ro = em1.find(Goblin.class, "Imie3");
        Goblin g4_ro = em1.find(Goblin.class, "Imie4");
        Goblin g5_ro = em1.find(Goblin.class, "Imie5");

        em1.getTransaction().commit();
        em1.close();
/*
        System.out.println("--------");
        System.out.println(w1_ro);
        System.out.println("\t" + w1_ro.getGoblin());
        System.out.println(w2_ro);
        System.out.println("\t" + w2_ro.getGoblin());
        System.out.println(w3_ro);
        System.out.println("\t" + w3_ro.getGoblin());
        System.out.println("--------");

        System.out.println("--------");
        System.out.println(g1_ro);
        System.out.println(g2_ro);
        System.out.println(g3_ro);
        System.out.println(g4_ro);
        System.out.println(g5_ro);
        System.out.println("--------");

        System.out.println(empty);
        empty.getGoblins().forEach(goblin -> System.out.println("\t" + goblin));
        System.out.println(full);
        full.getGoblins().forEach(goblin -> System.out.println("\t" + goblin)); */

        CaveDao cDao = new CaveDao();

        List<Cave>caves = cDao.findAllCaves(emf);
        caves.forEach(c-> System.out.println(c));

        Long countCaves = cDao.countCaves(emf);
        System.out.println("Count quantity: "+countCaves);

        GoblinDao gDao = new GoblinDao();

        List<Goblin>goblins = gDao.findAllGoblins(emf);
        goblins.forEach(g-> System.out.println(g));

        Long countGoblins = gDao.countCaves(emf);
        System.out.println("Goblins quantity: "+countGoblins);

        WeaponDao wDao = new WeaponDao();

        List<Weapon>weapons = wDao.findAllWeapons(emf);
        weapons.forEach(w-> System.out.println(w));

        Long countWeapons = wDao.countWeapons(emf);
        System.out.println("Weapons quantity: "+countWeapons);

        System.out.println("Homeless: ");
        List<Goblin>homelessGoblins = gDao.homelessGoblins(emf);
        homelessGoblins.forEach(g-> System.out.println(g));

        System.out.println("Weaponless: ");
        List<Goblin>weaponlessGoblins = gDao.weaponlessGoblins(emf);
        weaponlessGoblins.forEach(g-> System.out.println(g));

        System.out.println("Empty caves ");
        List<Goblin>caves2 = cDao.emptyCaves(emf);
        caves2.forEach(g-> System.out.println(g));



        emf.close();
    }
}
