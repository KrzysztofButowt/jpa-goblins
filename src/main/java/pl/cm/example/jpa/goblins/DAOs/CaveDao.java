package pl.cm.example.jpa.goblins.DAOs;

import lombok.NoArgsConstructor;
import pl.cm.example.jpa.goblins.Cave;
import pl.cm.example.jpa.goblins.Goblin;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@NoArgsConstructor
public class CaveDao {
    public void add (Cave cave, EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(cave);
        em.getTransaction().commit();
        em.close();
    }
    public void remove (Cave cave, EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(cave);
        em.getTransaction().commit();
        em.close();
    }
    public void update (Cave cave, EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(cave);
        em.getTransaction().commit();
        em.close();
    }
    public Cave find (Cave cave, EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Cave resultCave = em.find(Cave.class, cave.getId());
        em.getTransaction().commit();
        em.close();
        return  resultCave;
    }
    public List<Cave> findAllCaves (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Cave> query = builder.createQuery(Cave.class);
        Root<Cave> cave = query.from(Cave.class);
        query.select(cave);
        List <Cave> caves =  em.createQuery(query).getResultList();
        em.getTransaction().commit();

        em.close();
        return caves;
    }
    public long countCaves (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<Cave> cave = criteriaQuery.from(Cave.class);
        criteriaQuery.select(builder.count(cave));
        TypedQuery<Long> query = em.createQuery(criteriaQuery);
        Long result = query.getSingleResult();

        em.getTransaction().commit();
        em.close();
        return result;
    }
  /*  public List<Cave> emptyCaves (EntityManagerFactory emf){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Cave> query = builder.createQuery(Cave.class);
        Root<Cave> cave = query.from(Cave.class);
        query.select(cave);
        List <Cave> caves =  em.createQuery(query).getResultList();

        em.getTransaction().commit();
        em.close();
        return caves;
    }
*/

}
