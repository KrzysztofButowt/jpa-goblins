package pl.cm.example.jpa.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode
@ToString (exclude = "authors")
@Entity
@Getter
@Setter

public class Book {

    @Id
    private String isbn;

    private String title;


    @ManyToMany( fetch = FetchType.EAGER)
    private List<Author> authors = new ArrayList<>();

    public Book(String isbn, String title) {
        this.isbn = isbn;
        this.title = title;
    }

}
