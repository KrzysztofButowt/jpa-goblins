package pl.cm.example.jpa.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@EqualsAndHashCode
@ToString(exclude = "books")
@Entity
@Getter
@Setter
@NoArgsConstructor

public class Author {

    @Id
    private Integer id;

    private String name;

    private String surname;


    @ManyToMany(mappedBy = "authors", fetch = FetchType.EAGER)
    private List<Book> books = new ArrayList<>();;

    public Author(Integer id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

}
