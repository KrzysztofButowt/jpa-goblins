package pl.cm.example.jpa;

import pl.cm.example.jpa.model.Author;
import pl.cm.example.jpa.model.Book;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookPu");
        EntityManager em;

        em = emf.createEntityManager();
        em.getTransaction().begin();

        Author a1 = new Author(1, "Maja", "Kossakowska");
        Author a2 = new Author(2, "Jarosław", "Grzędowicz");

        em.persist(a1);
        em.persist(a2);

        Book b1 = new Book("1", "Ruda Sfora");
        Book b3 = new Book("3", "Ruda Sfora 2");
        Book b2 = new Book("2", "Popiół i Kurz");
        Book b4 = new Book("4", "Popiół i Kurz 2");

        b1.getAuthors().add(a1);
        b3.getAuthors().add(a1);
        b2.getAuthors().add(a1);
        b2.getAuthors().add(a2);
        b4.getAuthors().add(a1);
        b4.getAuthors().add(a2);

        em.persist(b1);
        em.persist(b2);
        em.persist(b3);
        em.persist(b4);

        em.getTransaction().commit();
        em.close();

        em = emf.createEntityManager();
        Book b_1 = em.find(Book.class, "1");
        Book b_2 = em.find(Book.class, "2");
        Book b_3 = em.find(Book.class, "3");
        Book b_4  = em.find(Book.class, "4");
        Author a_1 = em.find(Author.class, 1);
        Author a_2 = em.find(Author.class, 2);


        em.close();

        System.out.println(b_1);
        b_1.getAuthors().forEach(a-> System.out.println("\t"+a));
        System.out.println(b_2);
        b_2.getAuthors().forEach(a-> System.out.println("\t"+a));
        System.out.println(b_3);
        b_3.getAuthors().forEach(a-> System.out.println("\t"+a));
        System.out.println(b_4);
        b_4.getAuthors().forEach(a-> System.out.println("\t"+a));
        System.out.println(a_1);
        a_1.getBooks().forEach(a-> System.out.println("\t"+a));
        System.out.println(a_2);
        a_2.getBooks().forEach(a-> System.out.println("\t"+a));

        emf.close();

    }

}
